package com.example.fragments;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.example.fragments.ApiService.ApiService;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements Coordinator{

    FrameLayout container;
    Disposable disposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        disposable = ApiService.getPopularFilms("Alcoholic")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<RequestModel>() {
                               @Override
                               public void accept(RequestModel requestModel) throws Exception {
                                   Log.d("wwww", requestModel.getDrinks().get(1).getStrDrink()+"");
                               }
                           }, new Consumer<Throwable>() {
                               @Override
                               public void accept(Throwable throwable) throws Exception {

                               }
                           });

                        container = findViewById(R.id.root);
        if (savedInstanceState == null){
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
                initPortrait();
            } else {
                initLandscape();
            }
        }else{
            //restore state
        }

    }
    private void initPortrait(){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root, new ButtonFragment())
            .commit();
    }
    private void initLandscape(){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root, new ButtonFragment())
                .commit();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root_right, TextFragment.newInstance(""))
                .commit();
    }
    @Override
    public void showNextFragment(String text) {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.root, TextFragment.newInstance(text))
                    .commit();
        }else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.root_right, TextFragment.newInstance(text))
                    .commit();
        }
    }
}
