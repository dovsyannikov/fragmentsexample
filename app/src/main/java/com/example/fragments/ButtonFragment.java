package com.example.fragments;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;


public class ButtonFragment extends Fragment {

    EditText editText;
    Coordinator coordinator;
    Button action;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Coordinator) {
            coordinator = (Coordinator) context;
        } else {
            throw new IllegalArgumentException(context.getClass().getSimpleName() + "must implement sn=mth");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_button, container, false);
        editText = root.findViewById(R.id.editText);
        action = root.findViewById(R.id.button);
        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                coordinator.showNextFragment(editText.getText().toString());
            }
        });
        return root;
    }
}
