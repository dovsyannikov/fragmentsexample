package com.example.fragments;

public interface Coordinator {
    void showNextFragment(String text);
}
