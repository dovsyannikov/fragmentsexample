package com.example.fragments;

import java.util.List;

public class RequestModel {
        List<Drinks> drinks;
        public static class Drinks{
                private String strDrink;
                private String strDrinkThumb;

                public String getStrDrink() {
                        return strDrink;
                }

                public void setStrDrink(String strDrink) {
                        this.strDrink = strDrink;
                }

                public String getStrDrinkThumb() {
                        return strDrinkThumb;
                }

                public void setStrDrinkThumb(String strDrinkThumb) {
                        this.strDrinkThumb = strDrinkThumb;
                }

                public Drinks(String strDrink, String strDrinkThumb) {
                        this.strDrink = strDrink;
                        this.strDrinkThumb = strDrinkThumb;
                }
        }

        public List<Drinks> getDrinks() {
                return drinks;
        }

        public void setDrinks(List<Drinks> drinks) {
                this.drinks = drinks;
        }
}
