//package com.example.fragments.Adapter;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import androidx.annotation.NonNull;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.example.hackathon3.Converter;
//import com.example.hackathon3.R;
//import com.example.hackathon3.models.RequestModel;
//import com.squareup.picasso.Picasso;
//
//import java.util.List;
//
//public class CoctailAdapter extends RecyclerView.Adapter<CoctailAdapter.Holder> {
//
//    public interface OnItemTouchListener {
//        void onClick(RequestModel.Film film);
//    }
//
//    Context context;
//    List<RequestModel.Film> movies;
//    private OnItemTouchListener listener;
//
//    public void setListener(OnItemTouchListener listener) {
//        this.listener = listener;
//    }
//
//    public CoctailAdapter(Context context, List<RequestModel.Film> movies) {
//        this.context = context;
//        this.movies = movies;
//    }
//
//    @NonNull
//    @Override
//    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        return new Holder(LayoutInflater.from(context).inflate(R.layout.film_main, parent, false));
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull Holder holder, int position) {
//        Picasso.get().load(Converter.convertImagePathToFullUrl(movies.get(position).getPoster_path())).into(holder.imageView);
//        holder.textView.setText(movies.get(position).getTitle());
//
//    }
//
//    @Override
//    public int getItemCount() {
//        return movies.size();
//    }
//
//    class Holder extends RecyclerView.ViewHolder {
//        ImageView imageView;
//        TextView textView;
//
//        public Holder(@NonNull View itemView) {
//            super(itemView);
//            imageView = itemView.findViewById(R.id.imageView);
//            textView = itemView.findViewById(R.id.textView);
//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    listener.onClick(movies.get(getAdapterPosition()));
//                }
//            });
//        }
//    }
//
//}
