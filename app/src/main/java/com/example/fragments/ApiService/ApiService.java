package com.example.fragments.ApiService;


import com.example.fragments.RequestModel;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class ApiService {
    private static final String API = "https://www.thecocktaildb.com/api/json/v1/1/";
    private static PrivateApi privateApi;

    public interface PrivateApi {

        //https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=Alcoholic

        //https://api.themoviedb.org/3/movie/{movie_id}/external_ids?api_key=<<api_key>>

        @GET("filter.php")
        Observable<RequestModel> getPopular(@Query("a") String typeOfDrink);

//        @GET("{movie_id}")
//        Observable<FilmInfo> getById(@Path("movie_id") String movieID, @Query("api_key") String api_key);

    }
    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(API)
                .client(client)
                .build();

        privateApi = retrofit.create(PrivateApi.class);
    }

    public static Observable<RequestModel> getPopularFilms (String typeOfDrink){
        return privateApi.getPopular(typeOfDrink);
    }

}
